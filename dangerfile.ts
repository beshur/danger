import { danger, message, warn } from "danger";

// const modifiedMD = danger.git.modified_files.join("- ");
// message("Changed Files in this PR: \n - " + modifiedMD);

if (danger.git.modified_files.includes("lang/en.json")) {
  if (!danger.git.modified_files.includes("lang/de.json")) {
    warn(
      "This PR changes translation files, do not forget to run translation script"
    );
  } else {
    message("Translation files seem synced");
  }
}
